//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//
// Original sources from the WEB. Author (s) unknown.
//------------------------------------------------------------------------------
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
//------------------------------------------------------------------------------
class infoVaultKeyAES {
	//--------------------------------------------------------------------------
	static String decrypt(String key, String encrypted) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		return new String(cipher.doFinal(hexToByteArray(encrypted)));
	}
	static int decrypti(String key, String encrypted) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		return Integer.parseInt(new String(cipher.doFinal(hexToByteArray(encrypted))));
	}
	//--------------------------------------------------------------------------
	static String encryptionKey(String key) {
		return !key.matches("\\p{XDigit}+") ? "" : ("0000000000000000" + key).substring(key.length());
	}
	//--------------------------------------------------------------------------
	private static byte[] hexToByteArray(String hex) {
		if (hex == null || hex.length() == 0)
			return null;
		byte[] $ = new byte[hex.length() / 2];
		for (int ¢ = 0; ¢ < $.length; ++¢)
			$[¢] = (byte) Integer.parseInt(hex.substring(2 * ¢, 2 * ¢ + 2), 16);
		return $;
	}
}
//==============================================================================
