//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.io.File;
import java.sql.*;
//------------------------------------------------------------------------------
class infoVaultKeyDB {
	private static final String homeDirectory = System.getProperty("user.home");
	private static final String slash = System.getProperty("file.separator");
	private static final String dbDirectory = ".goofy";

	private static final String[] dbNames = {"infoVaultIV", "goofyIV"};
	private static final String dbName = dbNames[infoVaultKey.releaseMarker];
	private static final String dbPath = homeDirectory + slash + dbDirectory;

	private static final String DBTABLE = "information";
	private static final String DBCOL1 = "name";
	private static final String DBCOL2 = "type";

	private static final String sqlGETINFO = "SELECT " + DBCOL1 + ", " + DBCOL2 + " FROM " + DBTABLE;

	private static Connection connection;
	private static Statement statement;
	private static ResultSet rs;
	//--------------------------------------------------------------------------
	static void connectDB() {
		File f = new File(dbPath + slash + dbName);
		if (! f.exists())
			infoVaultKey.Error("Database does not exist!!!");
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath + slash + dbName);
			statement = connection.createStatement();
		} catch (ClassNotFoundException e) {
			infoVaultKey.Error("Connection error (class not found): " + e.getMessage());
		} catch (SQLException e) {
			infoVaultKey.Error("Connection error ('create statement' SQL exception): " + e.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	static void closeDB() {
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			infoVaultKey.Error("Cannot close database: " + e.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	static void testEncryptionKey(String encryptionKey) throws Exception {
		rs = statement.executeQuery(sqlGETINFO);
		while (rs.next()) {
			infoVaultKeyAES.decrypt(encryptionKey, rs.getString(DBCOL1));
			infoVaultKeyAES.decrypti(encryptionKey, rs.getString(DBCOL2));
		}
		rs.close();
	}
}
//==============================================================================
