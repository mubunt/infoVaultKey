//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Scanner;
//------------------------------------------------------------------------------
class infoVaultKeyCompute {
	private static final int oneTraceValue1 = 1000;
	private static final int lineTraceValue1 = 100000;
	private static final int oneTraceValue2 = 1;
	private static final int lineTraceValue2 = 10;
	private static final String eol = System.getProperty("line.separator");
	private static final String sLegend01 = "Each character \"/\" will represent " + oneTraceValue1 + " checked combinations."
			+ eol + "One line will represent " + lineTraceValue1 + " checked combinations.";
	private static final String sLegend02 = "Each character \".\" will represent " + oneTraceValue2 + " checked combination."
			+ eol + "One line will represent " + lineTraceValue2 + " checked combinations.";
	private static final String sLegend03 = "Each character \"/\" will represent " + oneTraceValue1 + " checked combinations."
			+ eol + "One line will represent " + lineTraceValue1 + " checked combinations.";
	private static final String sLegend04 = "Each character \"/\" will represent " + oneTraceValue1 + " checked combinations."
			+ eol + "One line will represent " + lineTraceValue1 + " checked combinations.";
	private static String encryptionKey = "";
	private static int trialNumber;
	//--------------------------------------------------------------------------
	static void research(char[] e, int lkey) {
		System.out.println(sLegend01);
		infoVaultKeyDB.connectDB();
		LocalDateTime start = LocalDateTime.now();
		for (char element : e) if (combination(element + "", e, lkey)) break;
		LocalDateTime end = LocalDateTime.now();
		infoVaultKeyDB.closeDB();
		printResult(start, end);
	}
	static void research(Scanner stxt) {
		System.out.println(sLegend02);
		infoVaultKeyDB.connectDB();
		LocalDateTime start = LocalDateTime.now();
		while (stxt.hasNextLine()) {
			String s = stxt.nextLine();
			if (s.length() == 0) continue;
			trace(s, lineTraceValue2, oneTraceValue2);
			if (checkEncryptionKey(s)) {
				encryptionKey = s;
				break;
			}
		}
		LocalDateTime end = LocalDateTime.now();
		stxt.close();
		infoVaultKeyDB.closeDB();
		printResult(start, end);
	}
	static void research(Scanner stxt, char[] e, int nNumber) {
		System.out.println(sLegend03);
		infoVaultKeyDB.connectDB();
		LocalDateTime start = LocalDateTime.now();
		while (stxt.hasNextLine()) {
			String str = stxt.nextLine();
			if (str.length() == 0) continue;
			for (char element : e) if (combination(str + element, e, str.length() + nNumber)) break;
		}
		LocalDateTime end = LocalDateTime.now();
		stxt.close();
		infoVaultKeyDB.closeDB();
		printResult(start, end);
	}
	static void research(char[] e1, int nAlpha, char[] e2, int nNumber) {
		System.out.println(sLegend04);
		infoVaultKeyDB.connectDB();
		LocalDateTime start = LocalDateTime.now();
		for (char element : e1) if (combination(element + "", e1, nAlpha, e2, nNumber)) break;
		LocalDateTime end = LocalDateTime.now();
		infoVaultKeyDB.closeDB();
		printResult(start, end);
	}
	//--------------------------------------------------------------------------
	private static boolean combination(String s, char[] e, int maxsize) {
		if (s.length() > maxsize) return false;
		trace(s, lineTraceValue1, oneTraceValue1);
		if (checkEncryptionKey(s)) {
			encryptionKey = s;
			return true;
		}
		for (char element : e) if (combination(s + element, e, maxsize)) return true;
		return false;
	}
	private static boolean combination(String s, char[] e1, int maxalpha, char[] e2, int maxnum) {
		trace(s, lineTraceValue1, oneTraceValue1);
		if (checkEncryptionKey(s)) {
			encryptionKey = s;
			return true;
		}
		if (s.length() == maxalpha)
			for (char element : e2) {
				if (combination(s + element, e2, maxalpha + maxnum))
					return true;
				for (char elmnt : e1)
					if (combination(s + elmnt, e1, maxalpha, e2, maxnum))
						return true;
			}
		return false;
	}
	//--------------------------------------------------------------------------
	private static void printResult(LocalDateTime s, LocalDateTime t) {
		System.out.println(eol + "Start Time: " + s + eol + "End Time:   " + t + eol);
		System.out.println(encryptionKey.length() == 0 ? eol + "NOT FOUND" + eol
				: eol + "The encryption key is: " + encryptionKey.replaceFirst("^0+(?!$)", "") + eol);
	}
	//--------------------------------------------------------------------------
	private static boolean checkEncryptionKey(String Key) {
		boolean $ = true;
		try {
			infoVaultKeyDB.testEncryptionKey(infoVaultKeyAES.encryptionKey(Key));
		} catch (SQLException e) {
			infoVaultKey.Error("SQL error ('test encryption key' exception): " + e.getMessage());
		} catch (Exception e) {
			$ = false;
		}
		return $;
	}
	//--------------------------------------------------------------------------
	private static void trace(String s, int lineTraceValue, int oneTraceValue) {
		if (trialNumber == lineTraceValue) {
			System.out.println(" " + s);
			trialNumber = 0;
		}
		++trialNumber;
		if (trialNumber % oneTraceValue == 0) System.out.print("/");
	}
}
//==============================================================================
