//------------------------------------------------------------------------------
// Copyright (c) 2017-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
//------------------------------------------------------------------------------
class infoVaultKey {
	private static final int ENCRYPTIONKEY_LENGTH = 16;
	private static final String eol = System.getProperty("line.separator");
	private static final String intelliJobjpath = "infoVaultKey_java/infoVaultKey/out/production/infoVaultKey/";
	private static final String jarFile = "infoVaultKey.jar";
	private static final String toolName = "infoVaultKey";
	private static final String sStrategy0 = "Search a key made of characters belonging to the set [0 .. 9, A .. F]";
	private static final String sStrategy1 = "Search a key made of characters belonging to the set [A .. F, 0 .. 9]";
	private static final String sStrategy2 = "Search a key with only alphabetical characters";
	private static final String sStrategy3 = "Search a key with only numerical characters";
	private static final String sStrategy4 = "Search a key of a known maximal number of alphabetic characters followed by" +
			eol + "a known maximal number of numeric characters";
	private static final String sStrategy5 = "Search a key belonging to a dictionary";
	private static final String sStrategy6 = "Search a key belonging to a dictionary, followed by a known maximal number of" +
			eol + "numeric characters";
	private static final String sQuest0 = "Maximal number of characters for the key: ";
	private static final String sQuest1 = "Maximal number of alphabetic characters: ";
	private static final String sQuest2 = "Maximal number of numerical characters: ";
	private static final String sQuest3 = "Path to this dictionary: ";

	private static Display display;
	private static Font fontTitle, fontButton, fontLabel;

	static int releaseMarker;
	//--------------------------------------------------------------------------
	public static void main(String[] args) {
		//----------------------------------------------------------------------
		//---- PATH OF THIS JAR
		releaseMarker = 0;
		String InstallRootDir = infoVaultKey.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace(jarFile, "");
		if (InstallRootDir.length() > intelliJobjpath.length())
			if (intelliJobjpath.equals(InstallRootDir.substring(InstallRootDir.length() - intelliJobjpath.length())))
				releaseMarker = 1;
		//----------------------------------------------------------------------
		//---- OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("V", "version", false, "print the version information and exit");
		try {
			if (parser.parse(options, args).hasOption("version")) {
				System.out.println(jarFile +
				 	"	Version: " + infoVaultKeyBuildInfo.getVersion() +
					" - Build: " + infoVaultKeyBuildInfo.getNumber() +
					" - Date: " + infoVaultKeyBuildInfo.getDate());
				Exit(0);
			}
		} catch (ParseException exp) {
			Error("Parsing failed - " + exp.getMessage());
		}
		//----------------------------------------------------------------------
		//---- GRAPHICAL USER INTERFACE
		display = new Display();
		fontTitle = new Font(display, "Arial", 20, SWT.BOLD);
		fontButton = new Font(display, "Arial", 10, SWT.BOLD);
		fontLabel = new Font(display, "Arial", 10, SWT.NORMAL);

		Shell shell = new Shell(display, SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.BORDER);
		shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setText(infoVaultKey.toolName + ": RESEARCH STRATEGY");
		shell.setLayout(new FillLayout());
		shell.addListener(SWT.Close, event -> Exit(0));

		ExpandBar bar = new ExpandBar(shell, SWT.V_SCROLL);
		bar.setSpacing(8);
		bar.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		bar.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		//---- Strategy #1
		Composite composite = getComposite(bar);
		Text limit0 = getText(composite, sQuest0, ENCRYPTIONKEY_LENGTH + "");
		Button b0 = getRunButton(composite);
		getExpandItem(bar, sStrategy0, composite, 0);
		b0.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				int keylength = ENCRYPTIONKEY_LENGTH;
				if (limit0.getText().length() != 0) {
					int k = getNumber(limit0.getText());
					if (k <= 0) return;
					keylength = k;
				}
				char[] elements = fillElements('0', 10, 'a', 6);
				compute(elements, keylength);
			}
		});

		//---- Strategy #2
		composite = getComposite(bar);
		Text limit1 = getText(composite, sQuest0, ENCRYPTIONKEY_LENGTH + "");
		Button b1 = getRunButton(composite);
		getExpandItem(bar, sStrategy1, composite, 1);
		b1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				int keylength = ENCRYPTIONKEY_LENGTH;
				if (limit1.getText().length() != 0) {
					int k = getNumber(limit1.getText());
					if (k <= 0) return;
					keylength = k;
				}
				char[] elements = fillElements('a', 6, '0', 10);
				compute(elements, keylength);
			}
		});

		//---- Strategy #3
		composite = getComposite(bar);
		Text limit2 = getText(composite, sQuest0, ENCRYPTIONKEY_LENGTH + "");
		Button b2 = getRunButton(composite);
		getExpandItem(bar, sStrategy2, composite, 2);
		b2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				int keylength = ENCRYPTIONKEY_LENGTH;
				if (limit2.getText().length() != 0) {
					int k = getNumber(limit2.getText());
					if (k <= 0) return;
					keylength = k;
				}
				char[] elements = fillElements('a', 6, '0', 0);
				compute(elements, keylength);
			}
		});

		//---- Strategy #4
		composite = getComposite(bar);
		Text limit3 = getText(composite, sQuest0, ENCRYPTIONKEY_LENGTH + "");
		Button b3 = getRunButton(composite);
		getExpandItem(bar, sStrategy3, composite, 3);
		b3.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				int keylength = ENCRYPTIONKEY_LENGTH;
				if (limit3.getText().length() != 0) {
					int k = getNumber(limit3.getText());
					if (k <= 0) return;
					keylength = k;
				}
				char[] elements = fillElements('0', 10, 'a', 0);
				compute(elements, keylength);
			}
		});

		//---- Strategy #5
		composite = getComposite(bar);
		Text alphaNumber0 = getText(composite, sQuest1, ENCRYPTIONKEY_LENGTH + "");
		Text numNumber0 = getText(composite, sQuest2, ENCRYPTIONKEY_LENGTH + "");
		Button b4 = getRunButton(composite);
		getExpandItem(bar, sStrategy4, composite, 4);
		b4.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				int alpNumber = ENCRYPTIONKEY_LENGTH;
				if (alphaNumber0.getText().length() != 0) {
					int k = getNumber(alphaNumber0.getText());
					if (k <= 0) return;
					alpNumber = k;
				}
				int numNumber = ENCRYPTIONKEY_LENGTH - alpNumber;
				if (numNumber0.getText().length() != 0) {
					int k = getNumber(numNumber0.getText());
					if (k <= 0) return;
					numNumber = k;
				}
				char[] elements = fillElements('a', 6, '0', 0);
				char[] elements2 = fillElements('0', 10, 'a', 0);
				compute(elements, alpNumber, elements2, numNumber);
			}
		});

		//---- Strategy #6
		composite = getComposite(bar);
		Text dictionary0 = getText(composite, sQuest3, "");
		Button b5 = getRunButton(composite);
		getExpandItem(bar, sStrategy5, composite, 5);
		b5.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				Scanner txtscan;
				try {
					txtscan = new Scanner(new File(dictionary0.getText()));
				} catch (FileNotFoundException e) {
					WError("File opening failed - " + e.getMessage());
					return;
				}
				compute(txtscan);
			}
		});

		//---- Strategy #7
		composite = getComposite(bar);
		Text dictionary1 = getText(composite, sQuest3, "");
		Text numNumber1 = getText(composite, sQuest2, ENCRYPTIONKEY_LENGTH + "");
		Button b6 = getRunButton(composite);
		getExpandItem(bar, sStrategy6, composite, 6);
		b6.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				Scanner txtscan;
				try {
					txtscan = new Scanner(new File(dictionary1.getText()));
				} catch (FileNotFoundException e) {
					WError("File opening failed - " + e.getMessage());
					return;
				}
				int numNumber = 0;
				if (numNumber1.getText().length() != 0) {
					int k = getNumber(numNumber1.getText());
					if (k <= 0) return;
					numNumber = k;
				}
				char[] elements = fillElements('0', 10, 'a', 0);
				compute(txtscan, elements, numNumber);
			}
		});

		shell.setSize(600, 550);
		shell.setLocation((display.getBounds().width - shell.getBounds().width) / 2,
				(display.getBounds().height - shell.getBounds().height) / 2);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
		//----------------------------------------------------------------------
		//---- EXIT
		Exit(0);
	}
	//==========================================================================
	private static char[] fillElements(char c1, int n1, char c2, int n2) {
		char[] $ = new char[n1 + n2];
		int index = 0;
		while (index < $.length && index < n1) $[index++] = c1++;
		if (n2 != 0)
			while (index < $.length && index < (n1 + n2)) $[index++] = c2++;
		return $;
	}
	private static Composite getComposite(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		return composite;
	}
	private static Text getText(Composite parent, String sLabel, String defValue) {
		Label lab = new Label(parent, SWT.NONE);
		lab.setText(sLabel);
		lab.setFont(fontLabel);
		lab.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		GridData data0 = new GridData();
		data0.grabExcessHorizontalSpace = true;
		data0.horizontalAlignment = GridData.FILL;
		Text $ = new Text(parent, SWT.BORDER);
		$.setLayoutData(data0);
		$.setText(defValue);
		$.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		$.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		return $;
	}
	private static Button getRunButton(Composite parent) {
		new Label(parent, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		Button $ = new Button(parent, SWT.PUSH);
		$.setLayoutData(new GridData(SWT.END, SWT.BOTTOM, true, true, 1, 1));
		$.setText("Run");
		$.setFont(fontButton);
		$.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		$.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		return $;
	}
	private static int getNumber(String k) {
		int $;
		try {
			$ = Integer.parseInt(k);
		} catch (NumberFormatException e) {
			WError("Wrong value. Must be a positive number");
			return -1;
		}
		if ($ <= 0) {
			WError("Wrong value. Must be a positive number");
			return -1;
		}
		return $;
	}
	private static void getExpandItem(ExpandBar parent, String sLabel, Composite content, int i) {
		ExpandItem item0 = new ExpandItem(parent, SWT.NONE, i);
		item0.setText(sLabel);
		item0.setHeight(content.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		item0.setControl(content);
		// Cannot change fore and back ground colors. widgets are supplied by OS. Bug was recorded.....
		//item0.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		//item0.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
	}
	//==========================================================================
	private static void compute(char[] e, int lkey) {
		display.dispose();
		infoVaultKeyCompute.research(e, lkey);
		Exit(0);
	}
	private static void compute(Scanner stxt) {
		display.dispose();
		infoVaultKeyCompute.research(stxt);
		Exit(0);
	}
	private static void compute(Scanner stxt, char[] e, int nNumber) {
		display.dispose();
		infoVaultKeyCompute.research(stxt, e, nNumber);
		Exit(0);
	}
	private static void compute(char[] e1, int nAlpha, char[] e2, int nNumber) {
		display.dispose();
		infoVaultKeyCompute.research(e1, nAlpha, e2, nNumber);
		Exit(0);
	}
	//==========================================================================
	private static void WError(String s) {
		MessageBox messageBox = new MessageBox(display.getActiveShell(), SWT.OK | SWT.ICON_ERROR);
		messageBox.setMessage(s);
		messageBox.open();
	}
	//--------------------------------------------------------------------------
	static void Error(String ¢) {
		String sError = "!!!ERROR!!! ";
		System.err.println(sError + ¢);
		Exit(-1);
	}
	//--------------------------------------------------------------------------
	private static void Exit(int value) {
		if (fontTitle != null) fontTitle.dispose();
		if (fontButton != null) fontButton.dispose();
		if (fontLabel != null) fontLabel.dispose();
		if (display != null) display.dispose();
		System.exit(value);
	}
}
//==============================================================================
