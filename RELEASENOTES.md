# RELEASE NOTES: *infoVaultKey*, an application to help to retrieve its own infoVault encryption key

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.2.14**:
  - Updated version of third party jar files (swt).

- **Version 1.2.13**:
  - Updated build system components.

- **Version 1.2.12**:
  - Updated build system.

- **Version 1.2.11**:
  - Removed unused files.

- **Version 1.2.10**:
  - Updated build system component(s)

- **Version 1.2.9**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.2.8**:
  - Abandonned Windows support since version 1.2.6

- **Version 1.2.7**:
  - Updated SWT library from 4.12 to 4.15.

- **Version 1.2.6**:
  - Some minor changes in project structure and build.

- **Version 1.2.5 - Build 15:**
  - Fixed clean install for Windows.

- **Version 1.2.4 - Build 15:**
  - Fixed bug introduced in 1.2.2 when fixing compilation warnings!!!

- **Version 1.2.3 - Build 15:**
  - Modified background and foreground of the main "box" (expandbar widget) for more readability. However, the
    expanded part remains grey; this widget is supplied by OS; bug was recorded.....

- **Version 1.2.2 - Build 14:**
  - Improved ./Makefile, ./infoVaultKey_bin/Makefile, ./infoVaultKey_c/Makefile, ./infoVaultKey_java/Makefile.
  - Removed C compilation warnings.
  - Moved to SWT 4.12. Initially was 4.8.
  - Moved to SQLIT JDBC 3.27.2.1. Initially was 3.23.1.

- **Version 1.2.1 - Build 13:**
  - Removed generated jar files on *clean*.
  - Automated, as much as possible, the management of java libraries to facilitate their update.
  - Added version identification text file.
  - Updated libraries to their most recent versions
  - Moved from GPL v2 to GPL v3.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Replaced Release Nores file (this file) by markdown version.
  - Updated README.md file.

- **Version 1.2.0 - Build 12:**
  - Added ".comment" file in each directory for 'yaTree' utility.

- **Version 1.1.0 - Build 12:**
  - Driver is now a C executable. Was a script (bash / cmd) file.
  - Introduced cross generation for Windows (make wall|wclean|winstall|wcleaninstall).
  - Specified foreground and background colors after migration from Ubuntu/Mate
	  to XUbuntu/Xfce4 (slightly differents with both 2 window managers).

- **Version 1.0.2 - Build 11:**
  - Slightly improved java sources.

- **Version 1.0.1 - Build 10:**
  - Moved to SWT 4.6.3. Initially was 4.6.2.
  - Moved to Commons CLI 1.4 Initially was 1.3.1.

- **Version 1.0.0 - Build 10:**
  - First release.
